# Show help
default:
  @just --list --justfile {{ justfile() }}

# Test the whole playbook
setup:
  ansible-playbook main.yml -K

# Run linting on the whole repository
lint:
  ansible-lint -f pep8
